<?php
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "test";

$scriptname = $argv[0];
$tagname = $argv[1];

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
$sql = "INSERT INTO citest (commit_user,commit_message) VALUES ('$scriptname','$tagname')";

if ($conn->query($sql) === TRUE) {
    echo "New record created successfully\n";
    $sql = "SELECT * from citest";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
    // output data of each row
       while($row = $result->fetch_assoc()) {
        echo "commit_id: " . $row["commit_id"]. " - commit_time: [" . $row["commit_time"]. "] " . $row["commit_user"]. " - " . $row["commit_message"]. "\n";
       }
    } else {
    echo "0 results";
    }
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

$conn->close();
?>
