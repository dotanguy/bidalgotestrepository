CREATE TABLE `citest` (
  `commit_id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `commit_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `commit_user` text,
  `commit_message` text,
  PRIMARY KEY (`commit_id`)
) DEFAULT CHARSET=utf8;
